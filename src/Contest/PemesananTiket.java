package Contest;

import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...

    static String namaPemesan;

    static TiketKonser dipesan;
    
    
    public static void run() throws InvalidInputException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan nama pemesan : ");
        namaPemesan = sc.nextLine();
        if (namaPemesan.length() >= 10) {
            throw new InvalidInputException("Nama pemesan tidak boleh lebih dari 10 karakter");
        }
        System.out.print("Pilih jenis tiket :");
        System.out.println("\n1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. VVIP");
        System.out.println("masukkan pilihan :");
        int pilihan = sc.nextInt();
        if (pilihan < 1 || pilihan > 5) {
            throw new InvalidInputException("Pilihan tiket harus antara 1 hingga 5");
        }

        switch (pilihan) {
            case 1:
                dipesan = new CAT8("CAT8", 100);
                break;
            case 2:
                dipesan = new CAT1("CAT1", 150);
                break;
            case 3:
                dipesan = new FESTIVAL("FESTIVAL", 200);
                break;
            case 4:
                dipesan = new VIP("VIP", 300);
                break;
            case 5:
                dipesan = new VVIP("VVIP", 400);
                break;
            default:
                break;
        }
    }
}